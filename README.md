# Sharky-STM32Duino
This is the code repository for Arduino IDE examples in "Sharky MKR Programmer's Guide".
The PDF file is available at http://www.midatronics.com

The Sharky MKR board is based on the Sharky module that contain STM32WB55CE SoC from STMicroelectronics.

The STM32WB55CE system on chip is a highly integrated low power radio design with two core in a single SoC. 

One ARM Cortex-M4 32-bit core is dedicated to customer application and one ARM Cortex-M0+ 32-bit microprocessor runs the radio networking stack. 
