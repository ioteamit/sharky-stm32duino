/***************************************************************************
  This is a library for the BME680 gas, humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME680 Breakout
  ----> http://www.adafruit.com/products/3660

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define BME_CS 7 // SHARKY

#define SEALEVELPRESSURE_HPA (1013.25)


Adafruit_BME680 bme(BME_CS); // hardware SPI


void setup() {
  pinMode(BME_CS,OUTPUT);

  SerialUSB.begin();

  SerialUSB.println(F("BME680 test"));

  if (!bme.begin()) {
    while (1) {
      SerialUSB.println("Could not find a valid SPI BME680 sensor, check wiring!!");
      delay(2000);
    }
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
}

void loop() {
  if (! bme.performReading()) {
    SerialUSB.println("Failed to perform reading :(");
  delay(2000);
  return;
  }
  SerialUSB.print("Temperature = ");
  SerialUSB.print(bme.temperature);
  SerialUSB.println(" *C");

  SerialUSB.print("Pressure = ");
  SerialUSB.print(bme.pressure / 100.0);
  SerialUSB.println(" hPa");

  SerialUSB.print("Humidity = ");
  SerialUSB.print(bme.humidity);
  SerialUSB.println(" %");

  SerialUSB.print("Gas = ");
  SerialUSB.print(bme.gas_resistance / 1000.0);
  SerialUSB.println(" KOhms");

  SerialUSB.print("Approx. Altitude = ");
  SerialUSB.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  SerialUSB.println(" m");

  SerialUSB.println();
  delay(2000);
}
